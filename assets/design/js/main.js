$(function(){

    $(".media").hover(function(){
        $(this).find(".media-menu").fadeIn(120);
        $(this).addClass( "media-active" );
    }, function() {
        $(this).find(".media-menu").fadeOut(80);
        $(this).removeClass( "media-active" );
    });
    
    $(".collapse-btn").click(function(){
        $('body').find(".collapse-data").slideDown(120);
    });
    
    $(".alert-success").click(function(){
        $(this).slideUp(120);
    });
    $(".alert-success").delay(2000).slideUp(120);
    
    
    

});

