var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var express = require('express');
var mongoose = require('mongoose');
var passwordHash = require('password-hash');

var User = mongoose.model('User', { name: String, email: String, password: String, lastStatus: String, lastSeen: String, isAdmin: Boolean });
var InvitationKey = mongoose.model('InvitationKey', { key: String, createdBy: String });
var Channel = mongoose.model('Channel', { name: String, createdBy: String, Created: String, isPrivate: Boolean });
var ChannelUser = mongoose.model('ChannelUser', { channel: String, user: String, Created: String });
var Message = mongoose.model('Message', { msg: { type: [String], index: true }, createdBy: String, Created: String, Channel: String });

app.use(express.static(process.cwd() + '/public'));

mongoose.connect('mongodb://localhost/robotalk');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
});

app.get('/', function(req, res){
  res.sendFile(__dirname + '/views/index.html');
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

io.on('connection', function(socket){
    
  socket.join('general');
  io.to('general').emit('some event','test');
    
  console.log('a user connected');
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });

  socket.on('msg', function(msg){
    console.log('message: ' + msg);
    socket.broadcast.emit('msg',msg);
    //socket.emit('msg',msg);
    return true; 
  });

  socket.on('login', function(array){
  
    User.findOne({ 'email': array[0] }, 'name password email lastStatus lastSeen isAdmin', function (err, UserOb) {
    
    if (typeof UserOb !== 'undefined' && UserOb !== null){
        if(passwordHash.verify(array[1], UserOb.password)){
            console.log('User Login: %s', UserOb.name);
            socket.emit('loginRes', UserOb);
            socket.broadcast.emit('notifyAlert',UserOb.name);
            return true; 
        }else{
            console.log('Login attempt, wrong password: %s', array[0]);
            socket.emit('loginRes', false);
            return false;
        }
        
    }else{
        console.log('Login attempt, user not found: %s', array[0]);
        socket.emit('loginRes', false);
        return false; 
    }
    
    });
  });
});



/*


var User1 = new User({ name: 'admin', email: 'test', password: passwordHash.generate('test'), lastStatus: 'test', lastSeen: '1412180887', isAdmin: true });
var Channel1 = new Channel({ name: 'random', createdBy: 'xxx', Created: 'xxx', isPrivate: false });
var Channel2 = new Channel({ name: 'general', createdBy: 'xxx', Created: 'xxx', isPrivate: false });



User1.save(function (err) {
  if (err) {
    console.log(err);
  } else {
    console.log('User registration');
  }
});

Channel1.save(function (err) {
  if (err) {
    console.log(err);
  } else {
    console.log('Channel created');
  }
});

Channel2.save(function (err) {
  if (err) {
    console.log(err);
  } else {
    console.log('Channel created');
  }
});*/