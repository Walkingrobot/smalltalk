$(function () {

    

    $(".collapse-btn").click(function () {
        $('body').find(".collapse-data").slideDown(120);
    });

    

    var socket = io();
    $("#login").click(function (e) {
        socket.emit('login', [$("#inputEmail").val(), $("#inputPassword").val()]);
        $("#inputPassword").val('');
        return false;
    });


    $("#msg").keyup(function (e) {
        if (e.keyCode == 13) {
            socket.emit('msg', $(this).val());
            setMedia($(this).val());
            $(this).val('');
            return false;
        }
    });
    
    $('#myModal').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    })
    
    function setMedia(msg){
        var template = $('.media-template').clone();
        template.attr('class','media');
        template.find('.media-heading').html(msg);
        template.appendTo('.media-container');
    }
    
    function setAlert(msg,type){
        $('#flash-msg').html(msg);
        $('#flash-msg').attr('class','alert alert-'+type);
        $('#flash-msg').slideDown();
        $('#flash-msg').delay(4000).slideUp();
    }
    
    $("#flash-msg").click(function () {
        $(this).slideUp(120);
    });
    
    socket.on('msg', function (msg) {
        setMedia(msg);
    });
    
    socket.on('loginRes', function (msg) {
        if(msg == false){
            $("#loginFailure").slideDown();
        }else{
            $('#myModal').slideUp();
            $('.modal-backdrop').fadeOut();
            setAlert('Welcome '+msg.name,'success');
            $('#msg').focus();
        }
    });
});